#include <vector>
#include <iostream>
class MatrixGraph {
public:
    MatrixGraph(size_t vertices_count): edges(vertices_count) {
        for (auto& line: edges) {
            line.assign(vertices_count, false);
        }
    }

    ~MatrixGraph() = default;

    void AddEdge(int from, int to) {
        edges[from][to] = true;
    }

    int VerticesCount() const {
        return edges.size();
    }

    std::vector<int> GetNextVertices(int vertex) const {
        std::vector<int> result;
        for (int i = 0; i < edges.size(); i++) {
            if (edges[vertex][i] == true) {
                result.push_back(i);
            }
        }
        return result;
    }

    std::vector<std::vector<bool>> edges;
};


void dfs_aux(const MatrixGraph &graph, int vertex, std::vector<bool> &visited) {
    visited[vertex] = true;
    std::vector<int> next_vertices = graph.GetNextVertices(vertex);
    for (int child : next_vertices) {
        if (!visited[child]) {
            dfs_aux(graph, child, visited);
        }
    }

}

int dfs(const MatrixGraph &graph) {
    std::vector<bool> visited(graph.VerticesCount(), false);
    int edges = 0;
    for (int i = 0; i < graph.VerticesCount(); ++i) {
        if (!visited[i]) {
            dfs_aux(graph, i, visited);
        }
    }
    int vis = 0;
    for (int i = 0; i < graph.VerticesCount(); ++i) {
        if (visited[i]) {
            vis++;
        }
    }
    if (vis != graph.VerticesCount()) {
        return 0;
    } else {
        for (int i = 0; i < graph.VerticesCount(); ++i) {
            for (int j = i + 1; j < graph.VerticesCount(); ++j) {
                if (graph.edges[i][j]) {
                    edges++;
                }
            }
        }
    }
    if (edges != vis - 1) return 0;
    else {
        return 1;
    }
}


int main(int argc, char const *argv[]) {
    int n = 0, m = 0;
    std::cin >> n >> m;

    MatrixGraph graph(n);
    for (int i = 0; i < m; i++) {
        int from = 0;
        int to = 0;
        std::cin >> from >> to;
        graph.AddEdge(from, to);
    }
    std::cout << dfs(graph) << std::endl;
    return 0;
}

