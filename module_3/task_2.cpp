/*
Дан невзвешенный неориентированный граф. В графе может быть несколько кратчайших путей между какими-то вершинами.
Найдите количество различных кратчайших путей между заданными вершинами.
 Требования: сложность O(V+E).
Формат ввода.
v: кол-во вершин (макс. 50000),
n: кол-во ребер (макс. 200000),
n пар реберных вершин,
пара вершин u, w для запроса.
Формат вывода.
Количество кратчайших путей от u к w.
 */
#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>

class ListGraph {
public:
    ListGraph(size_t vertices_count) : vertices(vertices_count) {
    }

    void AddEdge(int from, int to) {
        vertices[from].push_back(to);
        vertices[to].push_back(from);
    }

    int VerticesCount() const {
        return vertices.size();
    }

    std::vector<int> GetNextVertices(int vertex) const {
        std::vector<int> result;
        result.resize(vertices[vertex].size());
        std::copy(vertices[vertex].begin(), vertices[vertex].end(), result.begin());
        return result;
    }

private:
    std::vector<std::vector<unsigned int>> vertices;
};

unsigned int count_shortest_paths(ListGraph graph, const unsigned int from, const unsigned int to) {
    std::vector<int> paths(graph.VerticesCount(), 0);
    std::vector<int> steps(graph.VerticesCount(), 0);
    std::queue<int> q;
    q.push(from);
    paths[from] = 1;
    while (!q.empty()) {
        std::vector<int> nextVertices;
        unsigned int v = q.front();
        q.pop();
        nextVertices = graph.GetNextVertices(v);
        for (auto i:nextVertices) {
            if (paths[i] == 0) {
                q.push(i);
                paths[i] = paths[v];
                steps[i] = steps[v] + 1;
            } else if (steps[i] == steps[v] + 1) {
                paths[i] += paths[v];
            }
        }
    }
    return paths[to];
}

int main(int argc, const char *argv[]) {
    unsigned int v = 0;
    std::cin >> v;
    ListGraph graph(v);
    unsigned int n = 0;
    std::cin >> n;
    for (unsigned int i = 0; i < n; i++) {
        int a = 0, b = 0;
        std::cin >> a >> b;
        graph.AddEdge(a, b);
    }
    unsigned int from, to;
    std::cin >> from >> to;
    std::cout << count_shortest_paths(graph, from, to);
    return 0;
}
