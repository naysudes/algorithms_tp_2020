/*
Требуется отыскать самый выгодный маршрут между городами.
Требования: время работы O((N+M)logN), где N-количество городов, M-известных дорог между ними.
Формат входных данных.
Первая строка содержит число N – количество городов.
Вторая строка содержит число M - количество дорог.
Каждая следующая строка содержит описание дороги (откуда, куда, время в пути).
Последняя строка содержит маршрут (откуда и куда нужно доехать).
Формат выходных данных.
Вывести длину самого выгодного маршрута.
*/
#include <iostream>
#include <set>
#include <vector>
#include <climits>

class ListGraph {
public:
    struct Edge {
        int to;
        int time;

        Edge(int v, int t) : to(v), time(t) {}

        Edge() : to(-1), time(-1) {}
    };

    ListGraph(size_t vertices_count) : vertices(vertices_count) {}

    void AddEdge(int from, int to, int time) {
        vertices[from].push_back(Edge(to, time));
        vertices[to].push_back(Edge(from, time));
    }

    int VerticesCount() const {
        return vertices.size();
    }

    std::vector<Edge> GetEdgesFromVertex(int vertex) const {
        std::vector<Edge> result(vertices[vertex].size());
        std::copy(vertices[vertex].begin(), vertices[vertex].end(), result.begin());
        return result;
    }

private:
    std::vector<std::vector<Edge>> vertices;
};


int find_shortest_path(ListGraph &graph, int from, int to) {
    int n = graph.VerticesCount();
    std::vector<int> times(n, INT_MAX);
    times[from] = 0;

    std::set<std::pair<int, int>> q;
    q.emplace(std::make_pair(times[from], from));

    while(!q.empty()) {
        int curr = q.begin()->second;
        q.erase(std::make_pair(times[curr], curr));
        auto edges = graph.GetEdgesFromVertex(curr);
        for (const auto& edge : edges) {
            if (times[edge.to] == INT_MAX) {
                times[edge.to] = times[curr] + edge.time;
                q.emplace(std::make_pair(times[edge.to], edge.to));
            } else if (times[curr] + edge.time < times[edge.to]) {
                q.erase(std::make_pair(times[edge.to], edge.to));
                times[edge.to] = times[curr] + edge.time;
                q.emplace(std::make_pair(times[edge.to], edge.to));
            }
        }
    }
    return times[to];
}

int main(int argc, char const *argv[]) {
    int n = 0, m = 0;
    std::cin >> n >> m;

    ListGraph graph(n);
    for (int i = 0; i != m; i++) {
        int from = 0;
        int to = 0;
        int time = 0;
        std::cin >> from >> to >> time;
        graph.AddEdge(from, to, time);
    }

    int from = 0, to = 0;
    std::cin >> from >> to;
    std::cout << find_shortest_path(graph, from, to) << std::endl;
    return 0;
}
