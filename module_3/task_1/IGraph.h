#ifndef TP_ALGORITHMS_IGRAPH_H
#define TP_ALGORITHMS_IGRAPH_H

#include <iostream>
#include <vector>
#include <algorithm>
#include <cassert>

struct IGraph {
    virtual ~IGraph() = default;

    // Добавление ребра от from к to.
    virtual void AddEdge(int from, int to) = 0;

    virtual int VerticesCount() const  = 0;

    virtual std::vector<int> GetNextVertices(int vertex) const = 0;
    virtual std::vector<int> GetPrevVertices(int vertex) const = 0;

    bool operator==(const IGraph& right);
};


#endif //TP_ALGORITHMS_IGRAPH_H
