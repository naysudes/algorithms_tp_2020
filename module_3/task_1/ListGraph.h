#ifndef TP_ALGORITHMS_LISTGRAPH_H
#define TP_ALGORITHMS_LISTGRAPH_H

#include "IGraph.h"

/*ListGraph, хранящий граф в виде массива списков смежности
Также необходимо реализовать конструктор, принимающий const IGraph&.
Такой конструктор должен скопировать переданный граф в создаваемый объект.
Число вершин графа задается в конструкторе каждой реализации.*/
class ListGraph : public IGraph {
public:
    explicit ListGraph(size_t vertices_count);

    ListGraph(const IGraph &graph);

    ~ListGraph() = default;

    void AddEdge(int from, int to) override;

    int VerticesCount() const override;

    std::vector<int> GetNextVertices(int vertex) const override;

    std::vector<int> GetPrevVertices(int vertex) const override;

private:
    std::vector<std::vector<int>> vertices;
};

#endif  // TP_ALGORITHMS_LISTGRAPH_H
