//
// Created by naysudes on 27.05.2020.
//

#ifndef TP_ALGORITHMS_MATRIXGRAPH_H
#define TP_ALGORITHMS_MATRIXGRAPH_H

#include "IGraph.h"

/*MatrixGraph, хранящий граф в виде матрицы смежности*/
class MatrixGraph : public IGraph {
public:
    explicit MatrixGraph(size_t vertices_count);

    explicit MatrixGraph(const IGraph &graph);

    ~MatrixGraph() = default;

    void AddEdge(int from, int to) override;

    int VerticesCount() const override;

    std::vector<int> GetNextVertices(int vertex) const override;

    std::vector<int> GetPrevVertices(int vertex) const override;

private:
    std::vector<std::vector<bool>> edges;
};


#endif //TP_ALGORITHMS_MATRIXGRAPH_H
