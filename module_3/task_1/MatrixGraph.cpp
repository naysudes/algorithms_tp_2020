#include "MatrixGraph.h"
class MatrixGraph : public IGraph {
public:
    MatrixGraph(size_t vertices_count): edges(vertices_count) {
        for (auto& line: edges) {
            line.assign(vertices_count, false);
        }
    }


    ~MatrixGraph() = default;

    void AddEdge(int from, int to) {
        edges[from][to] = true;
    }

    int VerticesCount() const {
        return edges.size();
    }

    std::vector<int> GetNextVertices(int vertex) const {
        std::vector<int> result;
        for (int i = 0; i < edges.size(); i++) {
            if (edges[vertex][i] == true) {
                result.push_back(i);
            }
        }
        return result;
    }

    std::vector<int> GetPrevVertices(int vertex) const {
        std::vector<int> result;
        for (int i = 0; i < edges.size(); i++) {
            if (edges[i][vertex] == true) {
                result.push_back(i);
            }
        }
        return result;
    }

private:
    std::vector<std::vector<bool>> edges;
};
MatrixGraph::MatrixGraph(size_t vertices_count): edges(vertices_count) {
    for (auto& line: edges) {
        line.assign(vertices_count, false);
    }
}



void MatrixGraph::AddEdge(int from, int to) {
    edges[from][to] = true;
}

int MatrixGraph::VerticesCount() const {
    return edges.size();
}

std::vector<int> MatrixGraph::GetNextVertices(int vertex) const {
    std::vector<int> result;
    for (int i = 0; i < edges.size(); i++) {
        if (edges[vertex][i] == true) {
            result.push_back(i);
        }
    }
    return result;
}

std::vector<int> MatrixGraph::GetPrevVertices(int vertex) const {
    std::vector<int> result;
    for (int i = 0; i < edges.size(); i++) {
        if (edges[i][vertex] == true) {
            result.push_back(i);
        }
    }
    return result;
}