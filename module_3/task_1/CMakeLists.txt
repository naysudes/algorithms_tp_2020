cmake_minimum_required(VERSION 3.15)
project(tp_algorithms)

set(CMAKE_CXX_STANDARD 17)
add_executable(tp_algorithms main.cpp
        ListGraph.cpp
        ListGraph.h
        ArcGraph.cpp
        ArcGraph.h
        IGraph.h
        MatrixGraph.cpp
        MatrixGraph.h
        IGraph.cpp
        SetGraph.h
        SetGraph.cpp)