//
// Created by naysudes on 24.05.2020.
//

#ifndef TP_ALGORITHMS_ARCGRAPH_H
#define TP_ALGORITHMS_ARCGRAPH_H

/*ArcGraph, хранящий граф в виде одного массива пар {from, to}*/
#include "IGraph.h"

class ArcGraph: public IGraph {
public:
    explicit ArcGraph(size_t vertices_count);
    ArcGraph(const IGraph& graph);

    ~ArcGraph() = default;

    void AddEdge(int from, int to) override;
    int VerticesCount() const override;

    std::vector<int> GetNextVertices(int vertex) const override;

    std::vector<int> GetPrevVertices(int vertex) const override;

private:
    std::vector<std::pair<int, int>> edges;
    int vertices;
};


#endif //TP_ALGORITHMS_ARCGRAPH_H
