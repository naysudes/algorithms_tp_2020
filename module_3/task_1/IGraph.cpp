#include "IGraph.h"
bool IGraph::operator==(const IGraph& right) {
   if (VerticesCount() != right.VerticesCount()) {
       return false;
   }
    for (int i = 0; i < VerticesCount(); i++) {
        std::vector<int> next = GetNextVertices(i);
        std::vector<int> next_right = right.GetNextVertices(i);

        std::sort(next.begin(), next.end());
        std::sort(next_right.begin(), next_right.end());

        if (next != next_right) {
            return false;
        }

        std::vector<int> prev = GetPrevVertices(i);
        std::vector<int> prev_right = right.GetPrevVertices(i);

        std::sort(prev.begin(), prev.end());
        std::sort(prev_right.begin(), prev_right.end());

       if (prev != prev_right) {
            return false;
        }
    }
    return true;
}