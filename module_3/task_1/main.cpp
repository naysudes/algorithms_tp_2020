#include "ListGraph.h"
#include "MatrixGraph.h"
#include "SetGraph.h"
#include "ArcGraph.h"

void test_graph() {
    ArcGraph arc_graph(7);

    arc_graph.AddEdge(0, 1);
    arc_graph.AddEdge(1, 2);
    arc_graph.AddEdge(2, 3);
    arc_graph.AddEdge(2, 4);
    arc_graph.AddEdge(3, 4);
    arc_graph.AddEdge(1, 6);
    arc_graph.AddEdge(4, 2);
    arc_graph.AddEdge(5, 3);
    arc_graph.AddEdge(3, 5);
    arc_graph.AddEdge(1, 0);
    arc_graph.AddEdge(5, 2);
    arc_graph.AddEdge(6, 2);
    arc_graph.AddEdge(5, 4);

    MatrixGraph matrix_graph(arc_graph);
    ListGraph list_graph(matrix_graph);
    SetGraph set_graph(list_graph);

    if (arc_graph == matrix_graph && matrix_graph == list_graph && list_graph == set_graph) {
        std::cout << "all good!";
    } else {
        std::cout <<"something is off :(";
    }
}

int main() {
    test_graph();
    return 0;
}
