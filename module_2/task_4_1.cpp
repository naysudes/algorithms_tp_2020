//4_1. Солдаты. В одной военной части решили построить в одну шеренгу по росту. Т.к. часть была далеко не образцовая,
// то солдаты часто приходили не вовремя, а то их и вовсе приходилось выгонять из шеренги за плохо начищенные сапоги.
// Однако солдаты в процессе прихода и ухода должны были всегда быть выстроены по росту – сначала самые высокие,
// а в конце – самые низкие. За расстановку солдат отвечал прапорщик, который заметил интересную особенность –
// все солдаты в части разного роста. Ваша задача состоит в том, чтобы помочь прапорщику правильно расставлять солдат,
// а именно для каждого приходящего солдата указывать, перед каким солдатом в строе он должен становится.

#include <iostream>

template<class T>
struct DefaultComparator {
    int operator()(const T &l, const T &r) const {
        if (l < r) return -1;
        if (l > r) return 1;
        return 0;
    }
};

template<class Key, class Value, class Comparator=DefaultComparator<Key>>
class AVLTree {
    struct Node {
        Node *left;
        Node *right;

        Key key;
        Value value;
        u_int8_t height;
        int num_of_kids;

        Node(const Key &key, const Value &value)
                : left(nullptr), right(nullptr), key(key), value(value), height(1), num_of_kids(0) {
        }
    };

public:
    AVLTree(Comparator comp = Comparator()) : root(nullptr), comp(comp), nodes_count(0) {
    }

    ~AVLTree() {
        delete_tree(root);
    }

    void delete_tree(Node *node) {
        if (!node) {
            return;
        }
        delete_tree(node->left);
        delete_tree(node->right);
        delete node;
    }

    void insert(const Key &key, const Value &value, long &position) {
        root = insert_aux(root, key, value, position);
    }

    void erase(const Key &key) {
        root = erase_aux(root, key);
    }

    Key *find(long &pos) {
        return find_aux(root, pos);
    }

private:
    Node *balance(Node *node) {
        fix_height(node);
        fix_count(node);

        int bf = balance_factor(node);

        if (bf == 2) {
            if (balance_factor(node->right) < 0) {
                node->right = rotate_right(node->right);
            }
            return rotate_left(node);
        } else if (bf == -2) {
            if (balance_factor(node->left) > 0) {
                node->left = rotate_left(node->left);
            }
            return rotate_right(node);
        }
        return node;
    }

    int balance_factor(Node *node) {
        return height(node->right) - height(node->left);
    }

    void fix_height(Node *node) {
        node->height = std::max(height(node->left), height(node->right)) + 1;
    }

    void fix_count(Node *node) {
        int childCount = 0;

        if (node->right) {
            childCount++;
        }
        if (node->left) {
            childCount++;
        }
        childCount += count(node->right) + count(node->left);
        node->num_of_kids = childCount;

    }

    Key height(Node *node) {
        if (!node) {
            return 0;
        }
        return node->height;
    }

    int count(Node *node) {
        if (!node) {
            return 0;
        }
        return node->num_of_kids;
    }

    Key *find_aux(Node *node, long &pos) {
        if (!node) {
            return nullptr;
        }
        int current = 0;
        if (node->right) {
            current = count(node->right) + 1;
        }
        if (current == pos) {
            return &node->key;
        } else if (current > pos) {
            return find_aux(node->right, pos);
        } else if (current < pos) {
            pos = pos - current - 1;
            return find_aux(node->left, pos);
        }
    }

    Node *insert_aux(Node *node, const Key &key, const Value &value, long &position) {
        if (!node) {
            nodes_count++;
            return new Node(key, value);
        }
        int comp_res = comp(key, node->key);
        if (comp_res == -1) {
            if (node->right) {
                position += 2 + count(node->right);
            } else {
                position++;
            }
            node->left = insert_aux(node->left, key, value, position);
        } else {
            node->right = insert_aux(node->right, key, value, position);
        }
        return balance(node);
    }

    Node *erase_aux(Node *node, const Key &key) {
        if (!node) {
            return nullptr;
        }

        int comp_res = comp(key, node->key);
        if (comp_res == -1) {
            node->left = erase_aux(node->left, key);
        } else if (comp_res == 1) {
            node->right = erase_aux(node->right, key);
        } else {
            nodes_count--;
            Node *left = node->left;
            Node *right = node->right;
            Node* min_node = node;

            delete node;

            if (!right) {
                return left;
            }

            min_node->right = find_and_remove_min(right, min_node);

            min_node->left = left;

            return balance(min_node);
        }
        return balance(node);
    }

    Node *find_and_remove_min(Node *node, Node *result) {
        if (!node->left) {
            result = new Node(node->key, node->value);
            return node->right;
        }

        node->left = find_and_remove_min(node->left, result);
        return balance(node);

    }

    Node *rotate_right(Node *node) {
        Node *new_node = node->left;

        node->left = new_node->right;
        new_node->right = node;

        fix_count(node);
        fix_count(new_node);

        fix_height(node);
        fix_height(new_node);


        return new_node;
    }

    Node *rotate_left(Node *node) {
        Node *new_node = node->right;

        node->right = new_node->left;
        new_node->left = node;

        fix_count(node);
        fix_count(new_node);

        fix_height(node);
        fix_height(new_node);

        return new_node;
    }

    Node *root;
    Comparator comp;
    size_t nodes_count;
};

int main(int argc, const char *argv[]) {
    AVLTree<long, long> tree;
    size_t commands = 0;
    std::cin >> commands;
    size_t command;
    long height = 0;
    long position = 0;
    for (size_t i = 0; i < commands; i++) {
        std::cin >> command;
        switch (command) {
            case 1: {
                std::cin >> height;
                position = 0;
                tree.insert(height, height, position);
                std::cout << position << std::endl;
                break;
            }
            case 2: {
                position = 0;
                std::cin >> position;
                long *key = tree.find(position);
                if (key) {
                    tree.erase(*key);
                }
                break;
            }
            default:
                break;
        }
    }
    return 0;
}