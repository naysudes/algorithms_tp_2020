//Дано число N < 106 и последовательность целых чисел из [-231..231] длиной N.
//Требуется построить бинарное дерево, заданное наивным порядком вставки.
//Т.е., при добавлении очередного числа K в дерево с корнем root, если root→Key ≤ K,
// то узел K добавляется в правое поддерево root; иначе в левое поддерево root.
//Требования: Рекурсия запрещена. Решение должно поддерживать передачу функции сравнения снаружи.
//
//2_1. Выведите элементы в порядке in-order (слева направо).
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <stack>

template<class Value, class Comparator = std::less<Value>>
class BinarySearchTree {
public:
    struct Node {
        Node *left;
        Node *right;
        Value value;

        Node(const Value &value)
                : left(nullptr), right(nullptr), value(value) {}

        Node() : left(nullptr), right(nullptr), value(0) {}
    };

    BinarySearchTree(Comparator comp = Comparator()) : root(nullptr), comp(comp), nodes_count(0) {
    }

    ~BinarySearchTree() {
        if (!root) {
            return;
        }
        std::stack<Node *> stack;
        Node *current = root;
        Node *prev = nullptr;
        while (!stack.empty() || current) {
            while (current) {
                stack.push(current);
                current = current->left;
            }
            current = stack.top();
            prev=current;
            stack.pop();
            current = current->right;
            delete prev;
        }
    }

    bool add(const Value &value) {
        if (!root) {
            root = new Node(value);
            nodes_count++;
            return true;
        }
        Node *current = root;
        Node *prev = nullptr;
        while (current) {
            if (comp(current->value, value)) {
                prev = current;
                current = current->right;

            } else {
                prev = current;
                current = current->left;

            }
        }
        if (comp(prev->value, value)) {
            prev->right = new Node(value);
            nodes_count++;
            return true;
        } else {
            prev->left = new Node(value);
            nodes_count++;
            return true;
        }
    }
    void print_in_order() {
        if (!root) {
            return;
        }
        std::stack<Node *> stack;
        Node *current = root;
        while (!stack.empty() || current) {
            while (current) {
                stack.push(current);
                current = current->left;

            }
            current = stack.top();
            std::cout << current->value << " ";
            stack.pop();
            current = current->right;
        }
    }

private:
    Node *root;
    Comparator comp;
    size_t nodes_count;
};

int main() {
    BinarySearchTree<int> tree;
    size_t n = 0;
    int value;
    std::cin >> n;
    for (size_t i = 0; i < n; i++) {
        std::cin >> value;
        tree.add(value);
    }
    tree.print_in_order();
    return 0;
}