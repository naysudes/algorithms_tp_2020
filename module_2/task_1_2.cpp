//Реализуйте структуру данных типа “множество строк” на основе динамической хеш-таблицы с открытой адресацией.
//1_2. Для разрешения коллизий используйте двойное хеширование.

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

size_t str_hash(const std::string &key, const size_t a) {
    size_t h = 0;
    for (char i:key) {
        h = h * a + i;
    }
    return h;
}

size_t double_hash(const std::string &key, size_t i, size_t m) {
    size_t h1 = str_hash(key, 7);
    size_t h2 = str_hash(key, 13);
    h2 = h2 * 2 - 1;
    return (h1 + h2 * i) % m;
}

template<class T>
struct HashFunc;

template<>
struct HashFunc<std::string> {
    size_t operator()(const std::string &key, const size_t i, const size_t m) {
        return double_hash(key, i, m);
    }
};

template<class Hash = HashFunc<std::string>>
class HashTable {

    struct Element {
        const std::string empty = "EMPTY";
        const std::string del = "DELETED";
        std::string key;
        std::string value;

        Element() : key(empty), value(empty) {}

        Element(std::string &key, const std::string &value)
                : key(key), value(value) {}

        bool is_empty() const {
            return key == empty;
        }

        bool is_deleted() const {
            return key == del;
        }

        void set_deleted() {
            key = del;
        }

        void set_empty() {
            key = empty;
        }

        Element &operator=(const Element &r) {
            this->key = r.key;
            this->value = r.value;
            return *this;
        }

    };

public:
    HashTable(Hash hash = Hash())
            : buckets(8), items_count(0), hash(hash) {
    }

    bool insert(const std::string &key, const std::string &value) {
        if (4 * items_count > 3 * buckets.size()) {
            grow();
        }

        int deleted = -1;
        size_t step = 0;
        while (step < buckets.size()) {
            size_t hash_index = hash(key, step, buckets.size());

            if (buckets[hash_index].is_empty()) {
                buckets[hash_index].key = key;
                buckets[hash_index].value = value;
                items_count++;
                return true;
            }

            if (buckets[hash_index].key == key) {
                return false;
            }

            if (buckets[hash_index].is_deleted() && deleted == -1) {
                deleted = hash_index;
            }
            step++;
        }
        if (deleted != -1) {
            buckets[deleted].key = key;
            buckets[deleted].value = value;
            items_count++;
            return true;
        }

        return false;
    }


    bool find(std::string &key) {
        if (!items_count) {
            return false;
        }
        size_t idx = 0;
        size_t step = 0;
        while (step < buckets.size()) {
            idx = hash(key, step, buckets.size());
            if (buckets[idx].is_empty()) {
                return false;
            }
            if (buckets[idx].key == key) {
                return true;
            }
            step++;
        }
        return false;
    }

    bool erase(std::string &key) {
        if (!items_count) {
            return false;
        }
        size_t idx = 0;
        size_t step = 0;
        while (step < buckets.size()) {
            idx = hash(key, step, buckets.size());
            if (buckets[idx].is_empty()) {
                return false;
            }
            if (buckets[idx].key == key) {
                buckets[idx].set_deleted();
                items_count--;
                return true;
            }
            step++;
        }
        return false;
    }

    void print_bucket() {
        for (const auto i:buckets)
            std::cout << i.key << i.value << std::endl;
    }

private:
    void grow() {
        std::vector<Element> new_buckets(buckets.size() * 2);
        for (auto &i : new_buckets) {
            i.set_empty();
        }
        std::vector<Element> old_buckets(buckets);
        buckets = new_buckets;
        for (const auto &i : old_buckets) {
            if (!i.is_empty() && !i.is_deleted()) {
                items_count--;
                insert(i.key, i.value);
            }
        }
    }

    std::vector<Element> buckets;
    size_t items_count;
    Hash hash;
};

int main() {
    char operation = '\0';
    std::string key = "";
    HashTable<> hash;
    while (std::cin >> operation >> key) {
        bool res = false;
        switch (operation) {
            case '+':
                res = hash.insert(key, key);
                break;
            case '-':
                res = hash.erase(key);
                break;
            case '?':
                res = (hash.find(key));
                break;
            default:
                break;
        }
        if (res) {
            std::cout << "OK" << std::endl;
        } else {
            std::cout << "FAIL" << std::endl;
        }
    }
    return 0;
}
