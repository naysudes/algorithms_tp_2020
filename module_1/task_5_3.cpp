/*
На числовой прямой окрасили N отрезков. Известны координаты левого и правого концов каждого отрезка (Li и Ri).
 Найти длину окрашенной части числовой прямой.
*/

#include <iostream>

using namespace std;

template<class T>
struct Point {
    T coord;
    bool ends_with;

    bool operator<(const Point &right) const {
        return coord < right.coord;
    }
};

template<class T, class Comparator>
void merge(T *first, size_t first_len, T *second, size_t second_len, T *res, Comparator comp) {
    size_t i = 0;
    size_t j = 0;
    while (i < first_len && j < second_len) {
        if (comp(first[i], second[j])) {
            res[i + j] = first[i];
            i++;
        } else {
            res[i + j] = second[j];
            j++;
        }
    }
    if (i == first_len) {
        copy(second + j, second + second_len, res + i + j);
    } else {
        copy(first + i, first + first_len, res + i + j);
    }
}

template<class T, class Comparator = less<T> >
void merge_sort(T *arr, size_t size, Comparator comp = Comparator()) {
    if (size <= 1) {
        return;
    }
    size_t first_len = size / 2;
    size_t second_len = size - first_len;
    merge_sort(arr, first_len);
    merge_sort(arr + first_len, second_len);
    T *res = new T[size];
    merge(arr, first_len, arr + first_len, second_len, res, comp);
    copy(res, res + size, arr);
}

template<class T>
size_t painted_length(Point<T> *points, size_t n) {
    merge_sort(points, n);
    int length_sum = 0;
    int layer = 1;
    for (size_t i = 1; i < n; i++) {
        if (layer) {
            length_sum += points[i].coord - points[i - 1].coord;
        }
        if (points[i].ends_with) {
            layer--;
        } else {
            layer++;
        }
    }
    return length_sum;
}

int main() {
    size_t n = 0;
    cin >> n;
    n *= 2;
    auto *points = new Point<int>[n];
    for (size_t i = 0; i < n; i++) {
        cin >> points[i].coord;
        points[i].ends_with = i % 2;
    }
    cout << painted_length(points, n);
    delete[] points;
    return 0;
}
