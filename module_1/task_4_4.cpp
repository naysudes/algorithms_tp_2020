/*Решение всех задач данного раздела предполагает использование кучи, реализованной в виде класса.
 * Решение должно поддерживать передачу функции сравнения снаружи.
 * Куча должна быть динамической.
Дан массив натуральных чисел A[0..n), n не превосходит 10^8. Так же задан размер некоторого окна
 (последовательно расположенных элементов массива) в этом массиве k, k<=n.
 Требуется для каждого положения окна (от 0 и до n-k) вывести значение максимума в окне.
Требования: Скорость работы O(n log n), память O(n).
Формат входных данных. Вначале вводится n - количество элементов массива.
 Затем вводится n строк со значением каждого элемента. Затем вводится k  - размер окна.
Формат выходных данных. Разделенные пробелом значения максимумов для каждого положения окна.
 */
#include <cstdio>
#include <functional>
#include <iostream>
#include <cassert>

using namespace std;
template<class T1, class T2>
struct my_pair {
    T1 elem;
    T2 idx;
};

template<class T1, class T2>
ostream &operator<<(ostream &out, const my_pair<T1, T2> &pair) {
  out << pair.elem;
  return out;
}

template<class T1, class T2>
class PairComparator {
public:
    bool operator()(const my_pair<T1, T2> &l, const my_pair<T1, T2> &r) {
      return l.elem < r.elem;
    }
};

template<class T>
class Vector {
private:
    T *buff;
    size_t capacity;
    size_t size;

public:
    Vector() : buff(nullptr), capacity(0), size(0) {}

    Vector(T *arr, size_t size) {
      this->size = size;
      this->capacity = size;
      buff = new T[size];
      copy(arr, arr + size, buff);
    }

    ~Vector() {
      delete[] buff;
    }

    void extend() {
      T *new_buff = new T[capacity * 2];
      copy(buff, buff + size, new_buff);
      delete[] buff;
      buff = new_buff;
      capacity *= 2;
    }

    size_t get_size() {
      return size;
    }

    size_t get_capacity() {
      return capacity;
    }

    T &operator[](int idx) {
      return buff[idx];
    }

    Vector &operator=(const Vector &r) {
      if (this != &r) {
        this->size = r.size;
        this->capacity = r.capacity;
        this->buff = new T[size];
        std::copy(r.buff, r.buff + size, this->buff);
      }
      return *this;
    }

    void remove(size_t idx) {
      assert(idx < size);
      copy(buff + idx + 1, buff + size, buff + idx);
      size--;
    }

    void add(T elem) {
      if (size == capacity) {
        extend();
      }
      buff[size++] = elem;
    }
};

template<class T, class Comparator = PairComparator<int, size_t>>
class Heap {
private:
    Vector<T> buff;
    size_t heap_size;
    Comparator comp;

    void sift_up(int idx) {
      while (idx > 0) {
        int parent = (idx - 1) / 2;
        if (!comp(buff[parent], buff[idx])) {
          return;
        }
        swap(buff[parent], buff[idx]);
        idx = parent;
      }
    }

    void sift_down(int idx) {
      int left = 2 * idx + 1;
      int right = 2 * idx + 2;
      int largest = idx;

      if (left < heap_size && !comp(buff[left], buff[largest])) {
        largest = left;
      }
      if (right < heap_size && !comp(buff[right], buff[largest])) {
        largest = right;
      }

      if (largest != idx) {
        swap(buff[idx], buff[largest]);
        sift_down(largest);
      }
    }

    void heapify() {
      for (int i = heap_size / 2 - 1; i >= 0; --i) {
        sift_down(i);
      }
    }

public:
    Heap(T *arr, size_t size) {
      buff = Vector<T>(arr, size);
      heap_size = size;
      comp = Comparator();
      heapify();
    }

    ~Heap() = default;

    void push(const T &value) {
      buff.add(value);
      sift_up(size());
      heap_size++;
    }

    bool is_empty() const {
      return (!heap_size);
    }

    size_t size() const {
      return heap_size;
    }

    T extract_max() {
      assert(!is_empty());
      T res = buff[0];
      buff[0] = buff[buff.get_size() - 1];
      buff.remove(buff.get_size() - 1);
      heap_size--;
      if (!is_empty()) {
        sift_down(0);
      }
      return res;
    }

    T get_max() {
      assert(!is_empty());
      return buff[0];
    }

    T &operator[](size_t idx) {
      assert(idx < buff.get_size());
      return buff[idx];
    }

    void print_heap() {
      int level = 1;
      for (int i = 1; i <= heap_size; i++) {
        if (i == level) {
          cout << endl;
          level *= 2;
        }
        cout << buff[i - 1] << ' ';
      }
      cout << endl;
    }
};

template<class T1, class T2>
void print_window_max(size_t win_sz, my_pair<T1, T2> *arr, size_t arr_sz) {
  if (!win_sz || !arr_sz) {
    return;
  }
  Heap<my_pair<T1, T2>> window(arr, win_sz);
  cout << window.get_max() << ' ';
  for (size_t i = win_sz; i < arr_sz; i++) {
    while (!window.is_empty() && (window.get_max().idx <= (i - win_sz))) {
      window.extract_max();
    }
    window.push(arr[i]);
    cout << window.get_max() << ' ';
  }
}

int main() {
  size_t n = 0;
  size_t win_sz = 0;
  cin >> n;
  auto *arr = new my_pair<int, size_t>[n];
  for (size_t i = 0; i < n; i++) {
    cin >> arr[i].elem;
    arr[i].idx = i;
  }
  cin >> win_sz;
  print_window_max(win_sz, arr, n);
  delete[] arr;
  return 0;
}

