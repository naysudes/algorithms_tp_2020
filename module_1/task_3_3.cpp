/*
3_3. Реализовать очередь с помощью двух стеков. Использовать стек, реализованный с помощью динамического буфера.
Требования: Очередь должна быть реализована в виде класса. Стек тоже должен быть реализован в виде класса.
*/
#include <iostream>
#include <cassert>

using namespace std;

template<class T>
class Stack {
private:
    int buff_size;
    T *buff;
public:
    int top;

    explicit Stack(int size = 10) {
        top = -1;
        buff_size = size;
        buff = new int[buff_size];
    }

    ~Stack() {
        delete[] buff;
    }

    T pop() {
        assert(top != -1);
        return buff[top--];
    }

    void push(T elem) {
        if (top + 1 == buff_size) {
            int new_size = buff_size * 2;
            T *temp = new int[new_size];
            for (int i = 0; i < buff_size; i++) {
                temp[i] = buff[i];
            }
            delete[] buff;
            buff_size = new_size;
            buff = temp;
        }
        buff[++top] = elem;
    }

};

template<class T>
class Queue {
private:
    Stack<T> push_stack;
    Stack<T> pop_stack;
public:
    void push(T elem) {
        push_stack.push(elem);
    }

    T pop() {
        if (this->is_empty()) {
            return -1;
        }
        if (pop_stack.top == -1) {
            int n = push_stack.top;
            for (int i = 0; i <= n; i++) {
                pop_stack.push(push_stack.pop());
            }
        }
        T elem = pop_stack.pop();
        return elem;
    }

    bool is_empty() {
        return (pop_stack.top == -1 && push_stack.top == -1);
    }
};

bool testing_queue() {
    Queue<int> queue;
    int num_of_cmnd = 0;
    int command = 0;
    int value = 0;
    int result = 0;
    bool is_working = true;
    cin >> num_of_cmnd;
    for (int i = 0; i < num_of_cmnd; i++) {
        cin >> command >> value;
        switch (command) {
            case 2:
                if (value!= queue.pop()) {
                    is_working = false;
                }
                break;
            case 3:
                queue.push(value);
                break;
            default:
                assert(false);
        }
    }
    return is_working;
}

int main() {
    if (testing_queue()) {
        cout << "YES";
    } else {
        cout << "NO";
    }
    return 0;
}