/*
Даны неотрицательные целые числа n,k и массив целых чисел из [0..10^9] размера n.
 Требуется найти k-ю порядковую статистику. т.е. напечатать число, которое бы стояло на позиции
 с индексом k (0..n-1) в отсортированном массиве.

 Внимание! В этой задаче для каждого B[i] сначала нужно определить диапазон для бинарного поиска размером порядка k,
 * а потом уже в нем делать бинарный поиск
*/
#include <iostream>
#include <random>
#include <cstdlib>

using namespace std;

size_t pick_partition_rand(size_t begin, size_t end) {
    random_device rd;
    uniform_int_distribution<std::mt19937::result_type> dist(begin, end);
    return dist(rd);
}

template<class T, class Comparator>
int partition(T *arr, size_t l, size_t r, Comparator comp) {
    size_t pivot_idx = pick_partition_rand(l, r);
    int pivot = arr[pivot_idx];
    swap(arr[0], arr[pivot_idx]);
    size_t i = r;
    size_t j = r;
    while (j > 0) {
        if (!comp(arr[j], pivot)) {
            swap(arr[j--], arr[i--]);
        } else {
            j--;
        }
    }
    swap(arr[i], arr[0]);
    return i;
}

template<class T, class Comparator = less<T> >
void find_order_stat(T *arr, int n, int k, Comparator comp = Comparator()) {
    size_t l = 0;
    size_t r = n - 1;
    while (l < r) {
        T part = partition(arr, l, r, comp);
        if (part > k) {
            r = part;
        } else {
            l = part + 1;
        }
    }
}

int main() {
    size_t k = 0;
    size_t n = 0;
    cin >> n >> k;
    int* arr = new int[n];
    for (size_t i = 0; i < n; i++) {
        cin >> arr[i];
    }
    find_order_stat(arr, n, k);
    cout << arr[k] << ' ';
    delete[] arr;
    return 0;
}
