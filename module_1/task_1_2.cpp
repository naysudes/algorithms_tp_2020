#include <iostream>

using namespace std;

struct Point {
    int x;
    int y;

    Point() : x(0), y(0) {}

    Point(int x, int y) : x(x), y(y) {}

    Point &operator=(const Point &right) {
        if (this == &right) {
            return *this;
        }
        x = right.x;
        y = right.y;
        return *this;
    }
};

double trapeze_area(struct Point first, struct Point second) {
    return (second.x - first.x) * (second.y + first.y) * 0.5;
}

double polygon_area(int num_of_points) {
    double area = 0;
    struct Point a, b, c;
    cin >> a.x >> a.y;
    b = a;
    for (int i = 1; i < num_of_points; i++) {
        cin >> c.x >> c.y;
        area += trapeze_area(b, c);
        b = c;
    }
    area += trapeze_area(b, a);
    return area;
}

int main() {
    int num_of_points = 0;
    cin >> num_of_points;
    cout << polygon_area(num_of_points);
}