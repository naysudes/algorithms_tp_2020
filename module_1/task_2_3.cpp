/*2_3. Даны два массива неповторяющихся целых чисел, упорядоченные по возрастанию. A[0..n-1] и B[0..m-1]. n >> m.
 * Найдите их пересечение. Требования: Время работы: O(m * log k), где k - позиция элемента B[m-1] в массиве A..
 * В процессе поиска очередного элемента B[i] в массиве A пользуйтесь результатом поиска элемента B[i-1].
 * Внимание! В этой задаче для каждого B[i] сначала нужно определить диапазон для бинарного поиска размером порядка k,
 * а потом уже в нем делать бинарный поиск.
*/
#include <iostream>
#include <cassert>

using namespace std;

void read_array(int n, int *arr) {
    for (int i = 0; i < n; i++) {
        cin >> arr[i];
    }
}

void print_array(int n, const int *arr) {
    for (int i = 0; i < n; i++) {
        cout << arr[i] << ' ';
    }
}


size_t binary_search(const int *arr, size_t l, size_t r, int elem) {
    size_t first = l;
    size_t last = r;
    while (first < last) {
        size_t mid = (first + last) / 2;
        if (arr[mid] < elem) {
            first = mid + 1;
        } else {
            last = mid;
        }
    }
    return (first == r || arr[first] != elem) ? -1 : first;
}

void find_range(const int *arr, int elem, size_t n, size_t *l, size_t *r) {
    while (arr[*r] <= elem && *r < n) {
        *l = *r;
        *r *= 2;
        if (*r > n) {
            *r = n;
        }
    }
}

size_t find_common_elems(int n, int m, const int *a, const int *b, int *common) {
    size_t num_of_common = 0;
    size_t l = 0;
    size_t r = 1;
    size_t pos = 0;
    for (int i = 0; i < m; i++) {
        find_range(a, b[i], n, &l, &r);
        pos = binary_search(a, l, r, b[i]);
        if (pos != -1) {
            l = pos;
            common[num_of_common] = b[i];
            num_of_common++;
        }
    }
    return num_of_common;
}

int main() {
    size_t n = 0;
    size_t m = 0;
    cin >> n >> m;
    assert(n >= 0 && m >= 0);

    int *a = new int[n];
    read_array(n, a);

    int *b = new int[m];
    read_array(m, b);

    int *common = new int[m];
    int num_of_common = find_common_elems(n, m, a, b, common);
    if (num_of_common > 0) {
        print_array(num_of_common, common);
    }
    delete[]common;
    delete[] a;
    delete[] b;
    return 0;
}